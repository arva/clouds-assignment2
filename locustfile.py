import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    @task
    def hello_world(self):
        #For the first 3 exercises:
        #self.client.get("/0/3.14159")

        #For the function exercise
        self.client.get("?left=0&right=3.14159")