from flask import Flask
import math

app = Flask(__name__)

@app.route('/<left>/<right>')
def index(left, right):
    string = ""
    left = float(left)
    right = float(right)
    intervals = [10, 10**2, 10**3, 10**4, 10**5, 10**6]
    for interval in intervals:
        step = (right - left)/interval
        area = 0
        midpoint = step/2
        first_step = left + midpoint
        for subinterval in range(interval):
            actual_step = subinterval * step
            area += abs(math.sin((first_step + actual_step)))
        area *= step
        string += "For N = " + str(interval) + " Integration is: " + str(area) + "<br>"
    return string

if __name__ == '__main__':
	app.run(debug=True)