import logging
import math
import azure.functions as func

def index(left, right):
    string = ""
    left = float(left)
    right = float(right)
    intervals = [10, 10**2, 10**3, 10**4, 10**5, 10**6]
    for interval in intervals:
        step = (right - left)/interval
        area = 0
        midpoint = step/2
        first_step = left + midpoint
        for subinterval in range(interval):
            actual_step = subinterval * step
            area += abs(math.sin((first_step + actual_step)))
        area *= step
        string += "For N = " + str(interval) + " Integration is: " + str(area) + "\n"
    return string

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    left = req.params.get('left')
    right = req.params.get('right')
    if not left or not right:
        try:
            logging.info('Start')
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            left = req_body.get('left')
            right = req_body.get('right')

    if left and right:
        return func.HttpResponse(index(left, right))
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
             status_code=200
        )
