# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    output = []

    #get text from blob
    dataset = yield context.call_activity('GetInputDataFn', 'hey')

    #Mapper
    tasks = []
    for data in dataset:
        tasks.append(context.call_activity('MapperActivity', data))
    mapping = yield context.task_all(tasks)

    #Shuffler
    shufflerInput = list()
    for list_ in mapping:
        for map in list_:
            shufflerInput.append(map)

    shufflerOutput = yield context.call_activity('ShufflerActivity', shufflerInput)

    #Reducer
    tasks = []
    for shuffler in shufflerOutput:
        tasks.append(context.call_activity('ReducerActivity', shuffler))
    output = yield context.task_all(tasks)
    
    return [output]

main = df.Orchestrator.create(orchestrator_function)