# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

def main(listofwords: list) -> list:
    output = list()
    for word in listofwords:
        output.append((word[0], [1] * listofwords.count(word)))
    return output